// Área de triangulo com lados ab, bc, ca
double areaT(double ab, double bc, double ca) {
    double s = ab+bc+ca; // semi-perimetro
    return sqrt(s*(s-ab)*(s-bc)*(s-ca));
}
// Raio de circulo inscrito dentro de triangulo com lados ab, bc, ca
double rIncircle(double ab, double bc, double ca) {
    return areaT(ab, bc, ca) / ((ab+bc+ca)*0.5); }
// Raio de circulo inscrito dentro de triangulo com vertices a, b, c
double rInCircle(point a, point b, point c) {
    return rInCircle(dist(a, b), dist(b, c), dist(c, a)); }

// retorna 1 se existe um centro de um inCircle , ou 0 caso contrário
// ctr será o centro do inCircle e r será o mesmo que rInCircle
int inCircle(point p1, point p2, point p3, point &ctr, double &r) {
    r = rInCircle(p1, p2, p3);
    if (equ(r, 0.0)) return 0; // não tem centro
    line l1, l2;
    double rat = dist(p1, p2) / dist(p1, p3);
    point p = translate(p2, scale(vec(p2, p3), rat / (1 + rat)));
    ptsToLine(p1, p, l1);
    ratio = dist(p2, p1) / dist(p2, p3);
    p = translate(p1, scale(vec(p1, p3), rat / (1 + rat)));
    ptsToLine(p2, p, l2);
    areIntersect(l1, l2, ctr); // pega o ponto de intersecção
    return 1;
}
// Raio de circulo circunscrito no triangulo com lados ab, bc, ca
double rCircumCircle(double ab, double bc, double ca) {
    return ab * bc * ca / (4.0 * areaT(ab, bc, ca)); }
// Raio de circulo circunscrito no triangulo com vertices a, b, c
double rCircumCircle(point a, point b, point c) {
    return rCircumCircle(dist(a, b), dist(b, c), dist(c, a)); }
