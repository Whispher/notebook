bool equ(double x, double y) { return fabs(x - y) < EPS; }
bool men(double x, double y) { return x < y - EPS; }
bool mai(double x, double y) { return x > y + EPS; }
double degToRad(double theta) { return theta * PI /180.0; }
double halfHypot(double d1, double d2) { return d1 * d1 + d2 * d2; }

