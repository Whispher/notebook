//Verifica se ponto p está dentro, na borda, ou fora do circulo com centro c e raio r
int insideCircle(point p, point c, double r) {
    double d = dist(p, c);
    return men(d, r) ? 0 : (equ(d, r) ? 1 : 2); }

// Retorna no ponto c o centro de um circulo que contém o raior r e os
// pontos p1 e p2. Retorna o centro que está a direita do vetor p1->p2
bool circle2PtsRad(point p1, point p2, double r, point &c) {
    double det = r * r / halfHypot(p1.x - p2.x, p1.y - p2.y) - 0.25;
    if (det < 0.0) return false;
    c.x = (p1.x + p2.x) * 0.5 + (p1.y - p2.y) * sqrt(det);
    c.y = (p1.y + p2.y) * 0.5 + (p2.x - p1.x) * sqrt(det);
    return true; } // Para pegar o outro centro, troque p1 e p2
