struct line { double a, b, c; };
void ptsToLine(point p1, point p2, line &l) {
    if (eq(p1.x, p2.x)) { // Linha vertical
        l.a = 1.0; l.b = 0.0; l.c = -p1.x; // Padrao para linha vertical
    } else {
        l.a = - (p1.y - p2.y) / (p1.x - p2.x);
        l.b = 1.0; // Valor fixo, significa que linha nao e vertical
        l.c = -(l.a * p1.x) - p1.y;
    }
}

bool areParallel(line l1, line l2) {
    return (eq(l1.a,l2.a) && eq(l1.b,l2.b); }

//Retorna true se as linhas se intersectam e faz do ponto p a intersecao
bool areIntersect(line l1, line l2, point &p) {
    if (areParallel(l1, l2)) return false;
    p.x = (l2.b * l1.c - l1.b * l2.c) / (l2.a * l1.b - l1.a * l2.b);
    if (!eq(l1.b,0.0))
        p.y = -(l1.a * p.x + l1.c);
    else
        p.y = -(l2.a * p.x + l2.c);
    return true;
}

