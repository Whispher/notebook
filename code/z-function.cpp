vi zFunction(string text){
	int left = 0;
	int right = 0;
	vi Z(text.size());
	Z.clear();
	Z[0] = 0; // Some invalid random number
	for(int k = 1; k < text.size(); k++) {
		if(k > right) {
			left = right = k;
			while(right < text.size() && text[right] == text[right - left])
				right++;
			
			Z[k] = right - left;
			right--;
		} else {
			int k1 = k - left;
			if(Z[k1] < right - k + 1) {
				Z[k] = Z[k1];
			} else { //otherwise try to see if there are more matches.
				left = k;
				while(right < text.size() && text[right] == text[right - left])
					right++;
			
				Z[k] = right - left;
				right--;
			}
		}
	}
	return Z;
}