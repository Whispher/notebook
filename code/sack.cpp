vector <int> g[MAXN];
vector <ii> query[MAXN];
int level[MAXN];
int a[MAXN]; // letter in each node
int o[MAXN]; // partial answer till this moment
int sz[MAXN];
bool ans[MAXN]; // answer for i-th query
int count_level(int v, int parent) {
    level[v] = 1 + level[parent];
    sz[v] = 1;
    for (auto u: g[v])
        if (u != parent) {
            count_level(u, v);
            sz[v] += sz[u];
        }
    return sz[v];
}

vector<int> *nos[MAXN];

void sack(int v, int parent, bool keep) {
    int mx = -1, bigger = -1;
    for (auto u: g[v])
        if (u != parent && sz[u] > mx)
            mx = sz[u], bigger = u;
    for (auto u: g[v])
        if (u != parent && u != bigger)
            sack(u, v, 0);
    if (bigger != -1) {
        sack(bigger, v, 1);
        nos[v] = nos[bigger];
    } else nos[v] = new vector<int>();

    nos[v]->push_back(v);
    o[level[v]] ^= a[v];

    for (auto u: g[v])
        if (u != parent && u != bigger)
            for (auto nxt: * nos[u]) {
                nos[v]->push_back(nxt);
                o[level[nxt]] ^= a[nxt];
            }

    for (auto qq: query[v]) {
        if (o[qq.first] == (o[qq.first] & -o[qq.first])) {
            ans[qq.second] = true;
        }
    }

    if (!keep) {
        for (auto nxt: * nos[v])
            o[level[nxt]] ^= a[nxt];
    }
}

sack(1, 0, 1);

// Roma gives you m queries, the i-th of which consists 
// of two numbers vi, hi. Let's consider the vertices in 
// the subtree vi located at depth hi. Determine whether 
// you can use the letters written at these vertices to 
// make a string that is a palindrome. The letters that 
// are written in the vertexes, can be rearranged in any
// order to make a palindrome, but all letters should be used.

// Input
// ____________
6 5
1 1 1 3 3
zacccd
1 1
3 3
4 1
6 1
1 2

// Output
// ____________
Yes
No
Yes
Yes
Yes