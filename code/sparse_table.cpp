void build(int n){
    for(int i=0; i<n; i++) sp[0][i] = val[i];
    for(int j=1; j <= lg[n]; j++){
        for(int i=0; i + (1<<j) <= n; i++){
            sp[j][i] = max(sp[j-1][i], sp[j-1][i+(1<<(j-1))]);
        }
    }
}

int query(int l, int r){
    int k = lg[r-l+1];
    return max(sp[k][l], sp[k][r-(1<<k)+1]);
}