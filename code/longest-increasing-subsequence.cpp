// Retorna o comprimento da maior subsequncia crescente
int longestSubsequenceWithActualSolution(vi arr){
    vi T(arr.size());
    vi actualSolution(arr.size());
    for(int i=0; i < arr.size(); i++){
        T[i] = 1;
        actualSolution[i] = i;
    }
    
    for(int i=1; i < arr.size(); i++){
        for(int j=0; j < i; j++){
            if(arr[i] > arr[j]){
                if(T[j] + 1 > T[i]){
                    T[i] = T[j] + 1;
                    //set the actualSolution to point to guy before me
                    actualSolution[i] = j;
                }
            }
        }
    }
    
    //find the index of max number in T 
    int maxIndex = 0;
    for(int i=0; i < T.size(); i++)
        if(T[i] > T[maxIndex])
            maxIndex = i;
    
    //lets print the actual solution
    int t = maxIndex;
    int newT = maxIndex;
    do{
        t = newT;
        cout << arr[t] << " ");
        newT = actualSolution[t];
    }while(t != newT);
    cout << "\n";

    return T[maxIndex];
}