struct point { double x, y;
    point() { x = y = 0.0; }
    point(double _x, double _y) : x(_x), y(_y){};
    bool operator == (point o) const {
        return equ(x, o.x) && equ(y, o.y); }
    bool operator < (point o) const {
        return (eq(x, o.x)) ? (y < o.y) : (x < o.x); }
};
double dist(point p1, point p2){ //distancia entre pontos
    return hypot(p1.x - p2.x, p1.y - p2.y); }

//Rotacao em volta do ponto 0,0 em ang graus
point rotate(point p, double ang) { 
ang = degToRad(theta);
    return point(p.x * cos(ang) - p.y * sin(ang),
p.x * sin(ang) + p.y * cos(ang));
}
