ll query(int i){ 
    ll sum = 0;
    for(; i; i-=(i&(-i))) sum += st[i];
    return sum; 
} 
void update(int i, int val)  { 
    for(; i < MAXN; i+=(i&(-i))) st[i] += val;
} 
ll getInvCount(int n){ 
    int inv = 0;
    for (int i=n-1; i>=0; i--)    { 
        inv += query(arr[i]-1); 
        update(arr[i], 1); 
    }
    return inv; 
} 