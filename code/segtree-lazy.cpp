void propagate(int id){
	if (acum[id]) {
		st[id] += (r-l+1) * acum[id];
		if (l != r) {
			acum[id << 1] += acum[id];
			acum[(id << 1)|1] += acum[id];
		}
		acum[id] = 0;
	}
}
void update(int id, ll l, ll r, int i, int j, ll v){
	propagate(id);
	if (j < l || i > r) return;
	if (i <= l && r <= j){
		st[id] += (r-l+1) * v;
		if (l != r) {
			acum[id << 1] += v;
			acum[(id << 1)|1] += v;
		}
		return;
	}
	int m = (l+r)>>1;
	update(id << 1, l, m, i, j, v);
	update((id << 1)|1, m+1, r, i, j, v);
	st[id] = st[id<<1] + st[(id<<1)|1];
}

ll query(int id, ll l, ll r, int i, int j){
	propagate(id);
	if (j < l || i > r) return 0;
	if (i <= l && r <= j) return st[id];

	int m = (l+r)>>1;
	return (
		query(id << 1, l, m, i, j) +
		query((id << 1)|1, m+1, r, i, j)
	);
}
