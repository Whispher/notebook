struct vec { double x, y;
    vec(double _x, double _y) : x(_x), y(_y) {}
    vec(point a, point b) : x(b.x - a.x), y(b.y - a.y) {}
};
vec scale(vec v, double s) {//Multiplica vetor por s
    return vec(v.x * s, v.y * s); }
point translate(point p, vec v) { // Soma ponto p com vetor v
    return point(p.x + v.x , p.y + v.y); }
double dot(vec a, vec b) { //Produto escalar de vetores
    return (a.x * b.x + a.y * b.y); }
double norm_sq(vec v) { //Modulo do vetor ao quadrado
return halfHypot(v.x, v.y); }

//Distancia entre ponto e linha com pontos a e b.
//Pre: dot, norm_sql, translate, scale e dist
double distToLine(point p, point a, point b, point &c) { 
    // formula: c = a + u * ab
    vec ab = vec(a, b);
    double u = dot(vec(a, p), ab) / norm_sq(ab); //U é 
    c = translate(a, scale(ab, u)); // translate a to c
    return dist(p, c);
}
// Distancia entre ponto e segmento de linha.
//Pre: dot, norm_sql, translate, scale e dist 
double distToLineSegment(point p, point a, point b, point &c) {
    vec ab = vec(a, b);
    double u = dot(vec(a, p), ab) / norm_sq(ab);
    if (u < 0.0) c = point(a.x, a.y); // Mais perto de a
    else if (u > 1.0) c = point(b.x, b.y); // Mais perto de b
    else c = translate(a, scale(ab, u));
    return dist(p, c);
}
// Retorna angulo aob em radianos. Pre: dot, norm_sql
double angle(point a, point o, point b) {
    vec oa = vec(o, a), ob = vec(o, b);
    return acos(dot(oa, ob) / sqrt(norm_sq(oa) * norm_sq(ob))); }

double cross(vec a, vec b) { return a.x * b.y - a.y * b.x; }

// Verifica se o ponto r está a esquerda da reta formada pelo vetor p->q
// note: Para aceitar pontos colineares, temos que mudar o ‘> 0’
bool ccw(point p, point q, point r) {
	return cross(vec(p, q), vec(p, r)) > 0; }
// retorna true se o ponto r está na mesma linha que a linha pq
bool collinear(point p, point q, point r) {
	return equ(cross(vec(p, q), vec(p, r)), 0.0); }
