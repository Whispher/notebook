vector<int> graph[maxn];
int parent[maxn][lg_maxn], level[maxn];

void dfs(int u, int p){
    for(auto v: graph[u]){
        if(v != p){
            level[v] = level[u] + 1;
            parent[v][0] = u;
            dfs(v,u);
        }
    }
}

void build(){
    for(int j=1; j<=lg[n]; j++){
        for(int i=1; i<=n; i++){
            parent[i][j] = parent[parent[i][j-1]][j-1];
        }
    }
}

int lca(int u, int v){
    if(level[u] < level[v]) return lca(v, u);
    for(int i=lg[n]; i>=0; i--){
        if(level[u] - (1<<i) >= level[v]){
            u = parent[u][i];
        }
    }
    if(u == v) return u;
    for(int i=lg[n]; i>=0; i--){
        if(parent[u][i] != parent[v][i]){
            u = parent[u][i];
            v = parent[v][i];
        }
    }
    return parent[u][0];
}