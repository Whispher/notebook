
//	Properties of Centroid Tree
//1. The tree formed contains all the N nodes of the original tree.  
//2. The height of the centroid tree is at most O(logN).
//3. Consider any two arbitrary vertices A and B and the path between them (in the original tree)
//can be broken down into A-->C and C-->B where C is LCA of A and B in the centroid tree.

#include <bits/stdc++.h>

using namespace std;

#define max 100007
#define lg_max 18
#define inf 100000000

vector<int> tree[max];
int level[max];
int parent[lg_max][max];
int lg, n, m;

int colour[max];

int sub_tree[max];
bitset<max> is_centroid;
int centroid_parent[max];

//Lca
void dfs_root(int u, int p){
    for(auto v: tree[u]){
        if(v != p){
            level[v] = level[u] + 1;
            parent[0][v] = u;
            dfs_root(v, u);
        }
    }
}

void build_parent(){
    for(int j=1; j<=lg; j++){
        for(int i=1; i<=n; i++){
            parent[j][i] = parent[j-1][parent[j-1][i]];
        }
    }
}

int lca(int u, int v){
    if(level[u] < level[v]) return lca(v, u);
    for(int i=lg; i>=0; i--){
        if(level[u] - (1<<i) >= level[v]){
            u = parent[i][u];
        }
    }
    if(u == v) return u;
    for(int i=lg; i>=0; i--){
        if(parent[i][u] != parent[i][v]){
            u = parent[i][u];
            v = parent[i][v];
        }
    }
    return parent[0][u];
}

//Centroid decompostion
int dfs(int u, int p){
    sub_tree[u] = 1;
    for(auto v: tree[u]){
        if(v != p && !is_centroid[v]) sub_tree[u] += dfs(v, u);
    }
    return sub_tree[u];
}

int get_centroid(int u, int p, int tree_size){
    int heavy_child = -1;
    is_centroid[u] = true;
    for(auto v: tree[u]){
        if(!is_centroid[v] && v != p){
            if((sub_tree[v]<<1) > tree_size) is_centroid[u] = false;
            if(heavy_child == -1 || sub_tree[v] > sub_tree[heavy_child]) heavy_child = v;
        }
    }
    if(is_centroid[u]) return u;
    else return get_centroid(heavy_child, u, tree_size);
}

int decompose_tree(int root){
    int centroid = get_centroid(root, root, dfs(root, root));
    for(auto v: tree[centroid]){
        if(!is_centroid[v]){
            int sub_centroid = decompose_tree(v);
            centroid_parent[sub_centroid] = centroid;
        }
    }
    return centroid;
}

//Operations
void paint(int no, int colour_paint){
    if(!colour[no]) colour[no] = colour_paint;
    else{
        int dist1 = level[no] + level[colour[no]] - (level[lca(no, colour[no])] << 1);
        int dist2 = level[no] + level[colour_paint] - (level[lca(no, colour_paint)] << 1);
        if(dist2 < dist1) colour[no] = colour_paint;
    }
    if(!centroid_parent[no]) return;
    paint(centroid_parent[no], colour_paint);
}

int dist(int no, int no_query){
    int dist = inf;
    while(no){
        if(colour[no]){
            int dist_test = level[no_query] + level[colour[no]] - (level[lca(no_query, colour[no])] << 1);
            if(dist_test < dist) dist = dist_test;
        }
        no = centroid_parent[no];
    }
    return dist;

}

int main(){
    scanf("%d%d", &n, &m);
    for(int i=0; (1<<i) <= n; i++) lg++;
    for(int i = 1; i < n; i++){
        int u, v;
        scanf("%d%d", &u, &v);
        tree[u].push_back(v);
        tree[v].push_back(u);
    }
    dfs_root(1,1); build_parent();
    decompose_tree(1);
    
    paint(1,1);
    while(m--){
        int op, x;
        scanf("%d%d", &op, &x);
        if(op == 1) paint(x, x);
        else printf("%d\n", dist(x, x));
    }
}

// Xenia the programmer has a tree consisting of n nodes. We will consider the tree nodes indexed from 1 to n. We will also consider the first node to be initially painted red, and the other nodes — to be painted blue. The distance between two tree nodes v and u is the number of edges in the shortest path between v and u.
// Xenia needs to learn how to quickly execute queries of two types:
// 1.paint a specified blue node in red;
// 2.calculate which red node is the closest to the given one and print the shortest distance to the closest red node.
// Your task is to write a program which will execute the described queries.

// Input
//_________
5 4
1 2
2 3
2 4
4 5
2 1
2 5
1 2
2 5

// Output
//_________
0
3
2
