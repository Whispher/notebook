typedef struct query{
    int id;
    int left, right;
    int x;
}query;

int values[maxn], freq[maxn], answer[maxn];
query qry[maxn];
int n, q, groups; //groups = sqrt(n)

bool compare(query a, query b){
    if(a.left/groups < b.left/groups) return true;
    else if(a.left/groups == b.left/groups){
        if(a.right < b.right) return true;
        else return false;
    }else return false;
}

void add(int x){ freq[values[--x]]++; }

void remove(int x){ freq[values[--x]]--; }

void mos(){
    int L = 1, R = 0;
    int l, r;
    for(int i=0; i<q; i++){
        l = qry[i].left;
        r = qry[i].right;

        while(r > R) add(++R);
        while(r < R) remove(R--);

        while(l > L) remove(L++);
        while(l < L) add(--L);
        answer[qry[i].id] = freq[qry[i].x];
    }
}