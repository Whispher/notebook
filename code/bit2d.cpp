int bit[maxn][maxn];
int n, m;

int read(int idx_x, int idx_y){
    int soma = 0, x = idx_x, y;
    while(x != 0){
        y = idx_y;
        while(y != 0){
            soma += bit[x][y];
            y -= y & (-y);
        }
        x -= x & (-x);
    }
    return soma;
}

void update(int idx_x, int idx_y, int value){
    int x = idx_x, y;
    while(x <= n){
        y = idx_y;
        while(y <= m){
            bit[x][y] += value;
            y += y & (-y);
        }
        x += x & (-x);
    }
}