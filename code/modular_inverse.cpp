//Extended GCD
pair<long long, long long> extended_gcd(long long a, long long b) {
	if (b == 0) return make_pair(1, 0);
	pair<long long, long long> t = extended_gcd(b, a % b);
	return make_pair(t.second, t.first - t.second * (a / b));
}

long long modinverse(long long a, long long m) {
	return (extended_gcd(a, m).first % m + m) % m;
}