struct Node {
	ll value;
	Node *left, *right;
	Node(ll VALUE = 0):
		value(VALUE), left(NULL), right(NULL) {}
};

typedef Node *Foo;

ll merge(Foo left, Foo right) {
	if (!left)
		return right->value;
	if (!right)
		return left->value;
	return max(left->value, right->value);
}

void update(Foo &node, ll left, ll right, ll index, ll value) {
	if (!node)
		node = new Node();
	if (left == right)
		node->value = value;
	else {
		ll mid = (left + right) / 2;
		if (index <= mid) 
			update(node->left, left, mid, index, value);
		else
			update(node->right, mid + 1, right, index, value);
		node->value = merge(node->left, node->right);
	}
}

ll query(Foo node, ll left, ll right, ll from, ll to) {
	if (!node)
		return LLONG_MIN;
	if (right < from or left > to)
		return LLONG_MIN;
	if (left >= from and right <= to)
		return node->value;
	ll mid = (left + right) / 2;
	return max(query(node->left, left, mid, from, to), query(node->right, mid + 1, right, from, to));
}

Foo root = NULL;

