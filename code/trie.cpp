struct trie{
	map<char, trie> node;
	bool end_point;

	trie(): end_point(false) {}

	void add(const string &s, int i = 0){
		if(i == s.size())
			end_point = true;
		else
			node[s[i]].add(s, i+1);
	}

	bool check(const string &s, int i = 0){
		if(i == s.size())
			return true;
		else{
			if(node.count(s[i]))
				node[s[i]].check(s, i+1);
			else
				return false;
		}
	}
};