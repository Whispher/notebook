# Any line followed by a '#' character is ignored
# Section headings must be in square brackets
# Subsections within a section should follow the format:
#   [filename within code directory][tab character \t][subsection title]

[Geometry]
auxiliar.cpp	Auxiliares
pontos.cpp	Pontos
retas.cpp	Retas
vetores.cpp	Vetores
circulos.cpp	Circulos
triangulos.cpp	Triangulos
poligonos.cpp	Poligonos

[Numerical algorithms]
modular_inverse.cpp 	Modular Inverse with Extended GCD
fast_exp_mod.cpp	Fast Exponentiation with Mod

[Graph algorithms]
sack.cpp	Sack
lca.cpp		Lowest Common Ancestor
centroid_decomposition.cpp	Centroid Decomposition

[Data structures]
segtree_dynamic.cpp	Dynamic Segment Tree
segtree-lazy.cpp	Segment Tree + Lazy Propagation
sqrt_decomposition.cpp	SQRT Decomposition
sparse_table.cpp	Sparse Table
mos.cpp			Mo's Algorithm
mos_on_tree.cpp		Mo's Algorithm on Tree
bit2d.cpp		BIT 2D
bit+invcount.cpp	BIT + Inversion Count

[DP]
knapsack.cpp	Knapsack
Aho_korasick.cpp	Aho Korasick
Hanoi_tower.cpp		Hanoi Tower
edit_distance.cpp	Edit Distance
minimum_partition.cpp	Minimum Partition

[Math]
Chinese_theorem.cpp	Chinese Theorem
coin_change.cpp		Coin Change

[Flow]
Dinic_algorithm.cpp	Dinic Algorithm

[String]
trie.cpp	Trie Tree
kmp.cpp		KMP
z-function.cpp	zFunction
longest-increasing-subsequence.cpp	Longest Increasing Subsequence
longest-palindromic-subsequence.cpp	Longest Palindromic Subsequence
suffix-array.java	Suffix Array
suffix-tree.java	Suffix Tree

[Useful information]
grafos.txt		Grafos
geometria.txt		Geometria
matematica.txt		Matematica
